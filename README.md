Se creo aplicacion con los requerimientos solicitados en https://gitlab.sg-zinobe.com/oscar.mendez/challenge-python-l1

- El nombre de la base de datos se llama prueba.db
- El nombre de la tabla donde se almacena las columnas del DataFrame se llama regiones
- Los archivos .json creados por la aplicacion estan guardados en la carpeta JSON.
- Se crearon pruebas unitarias para las funciones obtener_regiones y obtener_paises