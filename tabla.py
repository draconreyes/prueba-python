import hashlib
import requests
import pandas as pd

from time import time
import sqlite3
from datetime import datetime

class tabla():
    tiempo_inicial = time()
    url = "https://restcountries-v1.p.rapidapi.com/all"
    con = sqlite3.connect('DB/prueba.db')
    cursorObj = con.cursor()
    def crear_tabla_bd(self):
        try:
            self.cursorObj.execute("CREATE TABLE regiones(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,Region VARCHAR(70) NOT NULL,City_name VARCHAR(70) NOT NULL,Languaje VARCHAR(70) NOT NULL,TIME VARCHAR(50) NOT NULL);")
            self.con.commit()
            print("Tabla creada.")
        except:
            print("Tabla creada")

    def obtener_regiones(self):
        headers = {
            'x-rapidapi-host': "restcountries-v1.p.rapidapi.com",
            'x-rapidapi-key': "e43652d1dfmsh1006cb68ee19a97p1e5551jsnaaf407833714"
        }
        response = requests.request("GET", self.url, headers=headers)
        df = pd.read_json(response.text)
        regionesdf = df['region'].value_counts().keys().tolist()
        regiones = []
        for i in regionesdf:
            if i != '':
                regiones.append(i)
        return regiones


    def obtener_paises(self,regiones):
        """
        Esta funcion retorna un arreglo que contiene
        el primer pais encontrado en el dataframe que esta dentro de cada region
        >>> t=tabla()
        >>> t.obtener_paises(['Africa', 'Americas', 'Europe', 'Asia', 'Oceania', 'Polar'])
        ['Algeria', 'Anguilla', 'Åland Islands', 'Afghanistan', 'American Samoa', 'Antarctica']
        >>> t.obtener_paises(['Africa'])
        ['Algeria']
        >>> t.obtener_paises(['Polar'])
        ['Antarctica']
        """
        paises = []
        for i in regiones:
            self.url= "https://restcountries.eu/rest/v2/region/"+i
            response = requests.get(self.url)
            df = pd.read_json(response.text)
            paises.append(df['name'].values[0])
        return paises

    def obtener_idiomas(self, paises):
        """
            Esta funcion retorna un arreglo que contiene el idioma de cada pais ingresado
            encriptado en SHA1
            >>> t=tabla()
            >>> t.obtener_idiomas(['Algeria', 'Anguilla', 'Åland Islands', 'Afghanistan', 'American Samoa', 'Antarctica'])
            ['af4f4762f9bd3f0f4a10caf5b6e63dc4ce543724', '649df08a448ee3fa90f3746baaf6b0907df42c91', '04a422d38c95415cece1ac86e1ad2a1030048c03', 'cc50b3253c9ec78d83c0178cbd9aff6a66d8ced8', '649df08a448ee3fa90f3746baaf6b0907df42c91', '649df08a448ee3fa90f3746baaf6b0907df42c91']
            >>> t.obtener_idiomas(['Algeria'])
            ['af4f4762f9bd3f0f4a10caf5b6e63dc4ce543724']
            >>> t.obtener_idiomas(['Antarctica'])
            ['649df08a448ee3fa90f3746baaf6b0907df42c91']
        """
        idiomas = []
        for i in paises:
            url = "https://restcountries.eu/rest/v2/name/"+i+"?fields=languages;"
            response = requests.get(url)
            df = pd.read_json(response.text)
            myhash = hashlib.sha1(df['languages'][0][0]['name'].encode())
            myhash.digest()
            idiomas.append(myhash.hexdigest())
        return idiomas

    def crear_tabla(self, regiones,paises,idiomas):
        tiempo_inicial_fila = time()
        data = pd.DataFrame(columns=('Region', 'City name', 'Languaje', 'Time'))
        for i in range(len(regiones)):
            tiempo_final=time()
            data.loc[len(data)]=[regiones[i],paises[i],idiomas[i],tiempo_final - tiempo_inicial_fila]
        print(data)
        print("Tiempo total:" + str(tiempo_final-self.tiempo_inicial))
        print("Tiempo promedio:" + str(data['Time'].mean()))
        print("Tiempo minimo:" + str(data['Time'].min()))
        print("Tiempo maximo:" + str(data['Time'].max()))
        self.registrar_bd(data)
        self.crear_json(data)
    def registrar_bd(self,data):
        self.crear_tabla_bd()
        try:
         for i in range(len(data.index)):
            self.cursorObj.execute("insert into regiones(Region,City_name,Languaje,TIME) values('" + str(data['Region'][i]) + "','" + str(data['City name'][i]) + "','" + str(data['Languaje'][i]) + "','" + str(data['Time'][i]) + "');")
            self.con.commit()
         print("Se ingreso registro exitosamente en la base de datos Prueba  en la tabla regiones")
        except:
            print ("No se pudo ingresar el registro en la base de datos")
        self.cursorObj.execute("SELECT * FROM regiones");
        self.con.commit()
        for i in self.cursorObj:
            print(i)
    def crear_json(self, data):
        now = datetime.now()
        ruta="Json/tabla"+str(now.microsecond)+"_"+str(now.minute)+"_"+str(now.hour)+".json"
        data.to_json(ruta);
        print("Se creo archivo json en la ruta:"+ruta)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
    n = tabla()
    regiones= n.obtener_regiones()
    paises=n.obtener_paises(regiones)
    idiomas=n.obtener_idiomas(paises)
    n.crear_tabla(regiones,paises,idiomas)


